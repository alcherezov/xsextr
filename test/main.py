# -*- coding: utf-8 -*- 
# загружаем необходимые библиотеки
from utils import termsortlib, plotter

# Открываем библиотеку макросечений 
#libname='test/VVER1000_KLN3.lib'
libname='test/FA_C200_wat_leg.lib '
# ftype - номер типа кассеты, данные для которой будут прочитаны
tlib = termsortlib(libname, ftype=1)

# Печатаем в консоль содержимое библиотеки макросечений
# Размерности входных и выходных параметров
print "Dimention of vectors of input and output parameters: %i, %i"%(tlib.xdim, tlib.ydim)

# Перечисление входных параметров и диапазон их изменения
print "\nSet of macro cross-xections and min/max values: "
for i in range(tlib.xdim):
	print "%10i %20s %10.6F %10.6F"%(i, tlib.xtitle[i].strip(), tlib.xmin[i], tlib.xmax[i])

# Перечисление выходных параметров
print "\nSet of functions: "
for i in range(tlib.ydim):
	print "%10i %20s"%(i, tlib.ytitle[i].strip())


# Отображаем графики функций f[ny](vec) и (f[ny]/dx[i])(vec) for all i
# загружаем открытую библиотеку в plotter
pl = plotter(tlib)

# x1, x2  - точки, через которые проходит линия
# ny  - номер компоненты векторной функции
# np  - количество расчетных точек
for i in range(tlib.ydim)[:5]:
    pl.draw(x1=[0,-3,0,0,0], x2=[0,3,0,0,0], ny=i, np=200)
#    pl.draw(x1=[0,-3,-2,0,0], x2=[0,3,2,0,0], ny=i, np=200)
#    pl.draw(x1=[0,-2,-2,0,0], x2=[0,2,2,0,0], ny=i, np=200)

#from numpy import array
#xdata = 0.5*array([tlib.xmin+tlib.xmax]*2)
#ydata, dydata = tlib.get_ydata(xdata)
#print xdata
#print ydata