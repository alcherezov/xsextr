# -*- coding: utf-8 -*- 
import sys, os
from numpy import array, arange, dtype, linspace
from numpy.linalg import norm
import matplotlib.pyplot as plt
from ctypes import *

libpath =  os.path.join(os.path.dirname(__file__), "../")

def normalize(x):
	xnorm = norm(x)
	if xnorm > 0: 
		return x/xnorm
	else:
		return x


class termsortlib:
	def __init__(self, libname, ftype):
		"""Open library 'termsortlib' and load functions
		xdim - dimention of input vector x
		ydim - dimention of input vector y
		"""
		if sys.platform == "win32":
			dll = CDLL(libpath+"bin/termsortlib.dll")
			self.openlib = dll.OPENLIB
			self.getdata = dll.GETDATA
		elif sys.platform == "linux2":
			dll = CDLL(libpath+"bin/libtermsortlib.so")
			self.openlib = dll.openlib_
			self.getdata = dll.getdata_
		else:
			print "ERROR: unknown platform"

		filename = c_char_p(libpath+libname.ljust(1000))

		xmin = arange(100,dtype="float64")
		xmax = arange(100,dtype="float64")
		xtitle = array(["".ljust(18)]*100,dtype=dtype(str))
		ytitle = array(["".ljust(18)]*100,dtype=dtype(str))
		xdim = c_int()
		ydim = c_int()

		self.openlib(filename,byref(xdim),byref(ydim),xmin.ctypes,xmax.ctypes,
			xtitle.ctypes,ytitle.ctypes, byref(c_int(ftype)))

		self.ftype = ftype
		self.xdim = xdim.value
		self.ydim = ydim.value
		self.xmin = xmin[:self.xdim]
		self.xmax = xmax[:self.xdim]
		self.xtitle = xtitle[:self.xdim]
		self.ytitle = ytitle[:self.ydim]

	def get_ydata(self,xdata,ifder=True):
		"""Fatch data from library
		xdata - vector of input parameters
		ifder - is neccesary to derivatives calculation 
		"""
		npoints = len(xdata)
		xdata = array(xdata,dtype="float64")
		ydata = arange(npoints*self.ydim,dtype="float64")
		dydata = arange(self.xdim*self.ydim*npoints,dtype="float64")

		self.getdata(xdata.ctypes, ydata.ctypes, dydata.ctypes, 
			byref(c_int(self.xdim)), byref(c_int(self.ydim)), byref(c_int(npoints)), 
			byref(c_int(self.ftype)), byref(c_bool(ifder)))

		ydata  = ydata.reshape((npoints,self.ydim)).transpose()
		dydata = dydata.reshape((npoints,self.ydim,self.xdim)).transpose((1,2,0))
		return ydata, dydata


class plotter:
	def __init__(self,tlib):
		self.tlib = tlib
		self.x2y = lambda x: 0.5 *(array(x) + 1) * (self.tlib.xmax-self.tlib.xmin) + self.tlib.xmin

	def draw(self,x1=None,x2=None,ny=0,np=100):
		"""Drawing funtion and all it's derivatives 
		nx - numbers of arguments 
		ny - number of function component
		np - number of points
		"""
		if not len(x1) == self.tlib.xdim: print 'ERROR: dimension of vectors x1 or x2 is wrong'

		nx = []
		for n in range(self.tlib.xdim):
			if not ((x1[n] == 0)and(x2[n] == 0)): nx.append(n)

		slinspace = linspace(0,1,np)

		y1, y2 = map(self.x2y, [x1,x2]) 
		xdata = y1 + array([slinspace]).transpose().dot(array([y2 - y1]))
		ydata, dydata = self.tlib.get_ydata(xdata)

		plt.figure(num=None, figsize=(14, 10), dpi=80, facecolor='w', edgecolor='k')
		plt.subplot(2,1,1)
		plt.plot(slinspace, ydata[ny], '-', linewidth=2)
		plt.grid(b=True)
		plt.xlabel(self.tlib.xtitle[nx])
		plt.ylabel(self.tlib.ytitle[ny])
		plt.subplot(2,1,2)
		for k,y in enumerate(dydata[ny]):
			plt.plot(slinspace, normalize(y), '-', linewidth=2,label=self.tlib.xtitle[k].strip())
			plt.grid(b=True)
		plt.ylabel(self.tlib.ytitle[ny])
		plt.xlabel(self.tlib.xtitle[nx])
		plt.legend()
	#	plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
	#	plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
	#          fancybox=True, shadow=True, ncol=5)
		plt.show()
