module features
  use precision, only: dp
  contains

  function normalize(v,dim) result (u)

    implicit none

    integer :: i, dim
    real(dp) :: v(dim), u(dim)

    u(:) = v(:)
    do i=1,xdim
      if (abs(v(i)) > 1) u(i) = v(i) / abs(v(i))
    enddo

    return
  end function


  function vector_incide(v,dim) result (incide)

    implicit none

    integer :: i, dim
    logical :: incide
    real(dp) :: v(dim)

    incide = .True.

    do i=1,xdim
      if (abs(s(i)) >= 1) then
        ifincide = .False.
        break
      endif
    enddo

    return
  end function


  function vector_angles(v,dim) result (theta)

    implicit none

    integer :: i, dim
    real(dp) :: v(dim), theta(dim), norm

    norm = vector_norm(v(:),dim)

    do i=1,dim
      theta(i) = arccos(v(i)/norm)
    enddo

    return
  end function



  function vector_scale(v,vmin,vmax,u,dim) result (u)

    implicit none

    integer :: i, dim
    real(dp) :: v(dim), vmin(dim), vmax(dim)
    real(dp) :: u(dim)

    do i=1,dim
      u(i) = 2*(v(i) - vmin(i))/(vmax(i) - vmin(i)) - 1
    enddo

    return
  end function


  function vector_norm(v,dim) result (norm)

    implicit none

    integer :: i, dim
    real(dp) :: v(dim)
    real(dp) :: norm

    do i=1,dim
      norm = norm + v(i) ** 2
    enddo
    norm = norm ** 0.5

    return
  end function


  function vector_getvalue(x,powers,factors,xdim,ydim,len) result (y)

    implicit none

    integer :: i, xdim, ydim, len
    integer :: powers(xdim,len)
    real(dp) :: x(xdim), y(ydim)
    real(dp) :: factors(ydim,len)

    y(:) = 0
    do j=1,len
      do i=1,xdim
        term = term * power(i,j) * ?
      enddo
      y(:) = y(:) + term * factors(:,j)
    enddo

    return
  end function



end module features



subroutine extrapolate(xdim, ydim, x, y, dy, powers, factors, len)

  use features, only

  implicit none

  integer :: xdim, ydim, len
  real(dp):: x(xdim)
  real(dp):: y(ydim), dy(xdim,ydim)
  real(dp):: powers(xdim,len), factors(ydim,len)

  real(dp):: alpha(xdim)
  real(dp):: s(xdim), sb(xdim), ds(xdim), theta(xdim), radius

  ! scaling of x vector
  s = vector_scale(x,xmax_scale(:,i_ftype),xmax_scale(:,i_ftype),xdim)
!  ifincide = vector_incide(s,xdim)

  sb(:) = normalize(s(:),xdim)
  ds(:) = s(:)-sb(:)

  radius = vector_norm(ds,xdim,2)  
  theta(:) = vector_angles(ds(:),xdim)
  alpha(:) = theta(:)/vector_norm(theta,xdim,1)

  yb = vector_getvalue(sproj,powers,factors,xdim,ydim,len)

  dy = vector_getderive(sproj,powers,factors,xdim,ydim,len)

  y(:) = yb(:) + vector_product(dy,ds*alpha,xdim,ydim) * ds(:)

end subroutine


