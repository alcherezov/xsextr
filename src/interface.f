subroutine getdata(xdata, ydata, dydata, xdim, ydim, npoints, i_ext_ftype, ifder)
!DEC$ ATTRIBUTES DLLEXPORT :: getdata
 
  use precision, only : dp
  use termsort_lib, only : ops_lib
  implicit none

  logical :: ifder ! do you need for calculate derivations into region or not
  integer :: i_ext_ftype, n ! fuel type number
  integer :: xdim, ydim     ! dimentions of input and output parameters
  integer :: npoints        ! number of input points
  real(dp) :: xdata(xdim,npoints)        ! set of input parameters
  real(dp) :: ydata(ydim,npoints)        ! set of output parameters
  real(dp) :: dydata(xdim,ydim,npoints)  ! derivatives of output parameters 

!  i_ext_ftype = 1

!  write(*,*) xdim, ydim, i_ext_ftype
!  pause

  do n=1,npoints
    call ops_lib(xdim, ydim, xdata(:,n), i_ext_ftype, ydata(:,n), dydata(:,:,n), ifder)
  enddo

end subroutine
 

subroutine openlib(filename,xdim,ydim,xmin,xmax,xtitle,ytitle,i_ext_ftype)
!DEC$ ATTRIBUTES DLLEXPORT :: openlib

  use precision, only : dp
  use termsort_lib, only : init_lib, get_lib_titles, get_lib_nx_ny, xmin_scale, xmax_scale, max_nx
  implicit none

  character*1000 :: filename
  integer :: xdim, ydim
  integer :: i_ext_ftype, i_xs_unit, NN_FTYPE
  character*18 :: xtitle(100), ytitle(100)
  real(dp) :: xmin(max_nx), xmax(max_nx)

!  i_ext_ftype = 1
  i_xs_unit = 1

! open and initialize library 'file_name' and close it
  open(i_xs_unit, file = filename, STATUS ='OLD')  
  call init_lib(i_xs_unit)
  close(i_xs_unit)

  xmin(:) = xmin_scale(:,1)
  xmax(:) = xmax_scale(:,1)

! receiving length of of X and Y arrays
  call get_lib_nx_ny(i_ext_ftype, xdim, ydim)

! receiving titles of T-H feedbacks
  call get_lib_titles(i_ext_ftype, xtitle, ytitle)

end subroutine
